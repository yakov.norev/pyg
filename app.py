
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from http.server import BaseHTTPRequestHandler, HTTPServer
from random import randint
import traceback
import os
import time
import random
import json
import datetime
import sys
import traceback
import mime
import html
import urllib

# print(__file__)
# print(os.path.realpath(__file__))
dir_path = os.path.dirname(os.path.realpath(__file__))
# print(dir_path)
# print(os.getcwd())
os.chdir(dir_path)
# print(os.getcwd())

configfile = './config.json'

with open(configfile, "r") as file:
    config = json.load(file)


def ExcexptonX():
    tt = sys.gettrace()
    tb = sys.exc_info()
    ff = tb[-1].tb_next.tb_frame.f_code
    # 'co_filename', 'co_firstlineno', 'co_name'
    g = ''
    g += str(ff.co_name) + " | "
    g += str(ff.co_filename) + " | "
    g += str(ff.co_firstlineno) + " | "
    g += tb[1].args[0] + " | "
    # g += (tb[-1].tb_frame.f_locals)['code']+" | "
    ff = tb[-1].tb_frame.f_code
    g += str(ff.co_name) + " | "
    g += str(ff.co_filename) + " | "
    g += str(ff.co_firstlineno) + " | "
    # g += tb[1].args[0] + " | "
    return g

class S:
    def __init__(self, x):
        self.x = x
        self.ms = list()
        self.hs = {}
        self.next = ''
        self.code = 200
        self.isExit = False

    def Code(self, code):
        self.code = code

    def Header(self, key, value):
        self.hs[key] = value

    def Send(self, o):
        self.ms.append(o)

    def G(self):
        return self.x.G()

    def Continue(self, next):
        self.next = next

    def Exit(self):
        self.isExit = True


class MyHandler(BaseHTTPRequestHandler):
    def pathX(self):
        self.path = config['public'] + urllib.parse.unquote(self.path)
        self.path = os.path.abspath(self.path)
        if os.path.exists(self.path) == False:
            self.send_response(404)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            return False

        if os.path.isdir(self.path):
            if self.path[-1:] != '/':
                self.path = self.path + '/'
            for d in config['defaults']:
                if os.path.isfile(self.path + d):
                    self.path = self.path + d
                    return True
            self.ListDir()
            return False
        return True

    def ListDir(self):
        dirs = os.listdir(self.path)
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        s = json.dumps({'dirs': dirs})
        self.wfile.write(bytes(s, 'UTF-8'))

    def CookiesX(self, s):
        s.cookies = {}
        if 'Cookie' in self.headers and type(self.headers['Cookie']) == str:
            for xcs in self.headers['Cookie'].split(';'):
                xx = xcs.split('=')
                s.cookies[xx[0].strip()] = xx[1]

    def FormX(self, s):
        s.form = {}
        if 'Content-Length' in self.headers and 'Content-Type' in self.headers and self.headers['Content-Type'] == 'application/x-www-form-urlencoded':
            a = self.rfile.read(
                int(self.headers['Content-Length'])).decode("utf-8")
            for xcs in a.split('&'):
                xx = xcs.split('=')
                s.form[xx[0].strip()] = xx[1]

    def ExcX(self, exc):
        excstr = ExcexptonX()
        self.send_response(500)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(bytes(str(excstr), 'UTF-8'))

    def ExecX(self, s):
        if self.path.endswith('.py'):
            code = open(self.path).read()
            try:
                exec(code, {'S': s})
                self.path = ""
                if s.next != '':
                    self.path = s.next
                    self.pathX()
                if s.isExit:
                    return True
            except Exception as exc:
                self.ExcX(exc)
                return True
        return False

    def G(self, s = None):
        try:
            if self.pathX() == False:
                return
            s = S(self) if s == None else s
            self.CookiesX(s)
            self.FormX(s)
            if self.ExecX(s):
                return

            if self.path != "":
                ContentType = 'application/octet-stream'
                for mimetype in mime.mimetypes:
                    if self.path.endswith(mimetype):
                        ContentType = mime.mimetypes[mimetype]

                s.Header('Content-type', ContentType)
                s.Send(open(self.path, "rb").read())

            self.send_response(s.code)
            for hk in s.hs.keys():
                self.send_header(hk, s.hs[hk])
            self.end_headers()
            for m in s.ms:
                m = m if type(m) == bytes else bytes(
                    m, 'UTF-8') if type(m) == str else bytes('#####', 'UTF-8')
                self.wfile.write(m)
        except Exception as exc:
            self.ExcX(exc)

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_POST(self):
        return self.G()

    def do_GET(self):
        return self.G()


if __name__ == '__main__':
    server_class = HTTPServer
    httpd = server_class(
        (config['HOST_NAME'], config['PORT_NUMBER']), MyHandler)
    print(time.asctime(), 'Server Starts - %s:%s' %
          (config['HOST_NAME'], config['PORT_NUMBER']))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), 'Server Stops - %s:%s' %
          (config['HOST_NAME'], config['PORT_NUMBER']))

# +';Expires=Wed, 21 Oct 2019 07:28:00 GMT;Path=/;HttpOnly'
