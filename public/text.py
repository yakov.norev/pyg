import json
import datetime

if 'username' in S.form or 'text' in S.form:
  datafile = './Data/msgs.json'
  msgs = {}
  try:
    with open(datafile, "r") as file:
      msgs = json.load(file)
  except BaseException as identifier:
    msgs = {}

  if ('msgs' in msgs) == False:
      msgs['msgs'] = list()

  msgs['msgs'].append({
      "user": S.form['username'],
      'text': S.form['text'],
      'date': str(datetime.datetime.now())})

  with open(datafile, "w") as file:
      file.write(json.dumps(msgs))

S.Code(302)
S.Header('Location', '/')
