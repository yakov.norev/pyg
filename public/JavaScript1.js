class Chat {
    constructor() {
        this.id = document.getElementById("chat");
        this.template = document.getElementById("chat_template");
    }
    Get() {
        this.template.removeAttribute("id");
        this.template.removeAttribute("hidden");
        fetch('/chat.py')
            .then(d => d.json())
            .then(d => {
            while (this.id.firstChild) {
                this.id.removeChild(this.id.firstChild);
            }
            var msgs = [];
            msgs.map(msg => this.id.appendChild(this.template.cloneNode(true)));
        });
    }
}
//# sourceMappingURL=JavaScript1.js.map